package com.blackstoneeit.enec.utils

import androidx.annotation.DrawableRes
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar

/**
 * Method to help us show the toolbar.
 * Note: This method should be called inside onCreate
 *
 *
 * @param inAppCompatActivity AppCompatActivity instance.
 * @param inToolbar           Reference to the toolbar view.
 */
fun showToolbar(
    @NonNull inAppCompatActivity: AppCompatActivity,
    @Nullable inToolbar: Toolbar,
    @DrawableRes backButtonSource: Int
) {
    inAppCompatActivity.setSupportActionBar(inToolbar)
    inAppCompatActivity.supportActionBar!!.setHomeAsUpIndicator(backButtonSource)
    inAppCompatActivity.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    inAppCompatActivity.supportActionBar!!.setDisplayShowHomeEnabled(true)
    inAppCompatActivity.supportActionBar!!.setDisplayShowTitleEnabled(true)
}