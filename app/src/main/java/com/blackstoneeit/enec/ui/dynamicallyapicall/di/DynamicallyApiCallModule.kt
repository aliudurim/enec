package com.blackstoneeit.enec.ui.dynamicallyapicall.di

import com.blackstoneeit.enec.di.scopes.ActivityScope
import com.blackstoneeit.enec.ui.dynamicallyapicall.DynamicallyApiCallActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DynamicallyApiCallModule {
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeDynamicallyApiCallActivityInjector(): DynamicallyApiCallActivity
}