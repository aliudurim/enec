package com.blackstoneeit.enec.ui.splash

import androidx.appcompat.app.AppCompatActivity
import com.blackstoneeit.enec.api.ApiService
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SplashPresenter @Inject constructor(
    private val apiService: ApiService
) : SplashContract.Presenter {

    private val subscriptions = CompositeDisposable()
    private lateinit var mView: SplashContract.View

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: SplashContract.View) {
        mView = view
    }

    override fun getActivity(activity: AppCompatActivity) {
        mView.showActivity(activity)
    }
}