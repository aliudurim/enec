package com.blackstoneeit.enec.ui.splash

import android.animation.Animator
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.blackstoneeit.enec.R
import com.blackstoneeit.enec.ui.behavior.BehaviorActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_splash.*
import javax.inject.Inject

class SplashActivity : AppCompatActivity(), SplashContract.View {
    @Inject
    lateinit var presenter: SplashContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_splash)
        presenter.attach(this)
        presenter.subscribe()
        imgSplashScreen.imageAssetsFolder = "images/"
        imgSplashScreen.enableMergePathsForKitKatAndAbove(true)
        imgSplashScreen.playAnimation()
        imgSplashScreen.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationRepeat(animation: Animator?) {

            }

            override fun onAnimationEnd(animation: Animator?) {
                presenter.getActivity(BehaviorActivity())
            }

            override fun onAnimationStart(animation: Animator?) {

            }
        })
    }

    override fun showProgress(show: Boolean) {

    }

    override fun showErrorMessage(error: String) {

    }

    override fun showActivity(activity: AppCompatActivity) {
        val intent = Intent(this@SplashActivity, activity::class.java)
        startActivity(intent)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribe()
    }
}