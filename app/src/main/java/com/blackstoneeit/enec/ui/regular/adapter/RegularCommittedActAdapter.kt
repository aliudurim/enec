package com.blackstoneeit.enec.ui.regular.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blackstoneeit.enec.R
import com.blackstoneeit.enec.models.CommittedAct
import kotlinx.android.synthetic.main.view_item_regular_committed_act.view.*

class RegularCommittedActAdapter :
    RecyclerView.Adapter<RegularCommittedActViewHolder>() {

    private val list: MutableList<CommittedAct> = mutableListOf(
        CommittedAct(false, "Select Profile")
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RegularCommittedActViewHolder {
        val view = RegularCommittedActViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.view_item_regular_committed_act, parent, false)
        )
        view.itemView.rcVRegularSubCommittedAct.layoutManager = LinearLayoutManager(parent.context)
        view.itemView.rcVRegularSubCommittedAct.adapter = RegularSubCommittedActAdapter()
        return view
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: RegularCommittedActViewHolder, position: Int) {
        holder.itemView.edRegularCommittedAct.text = list[holder.adapterPosition].categoryName
        if (list[holder.adapterPosition].expanded) {
            holder.itemView.cVRegularSubCommittedAct.visibility = View.VISIBLE
            holder.itemView.edRegularCommittedAct.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_arrow_up,
                0
            )
        } else {
            holder.itemView.cVRegularSubCommittedAct.visibility = View.GONE
            holder.itemView.edRegularCommittedAct.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_arrow_down,
                0
            )
        }
        holder.itemView.edRegularCommittedAct.setOnClickListener {
            if (list[holder.adapterPosition].expanded) {
                list[holder.adapterPosition].expanded = false
            } else {
                list.forEachIndexed { index, categoriesAndServices ->
                    if (categoriesAndServices.expanded) {
                        categoriesAndServices.expanded = false
                        notifyItemChanged(index)
                    }
                }
                list[holder.adapterPosition].expanded = true
            }
            notifyItemChanged(holder.adapterPosition)
        }
    }

}

class RegularCommittedActViewHolder(inView: View) : RecyclerView.ViewHolder(inView)