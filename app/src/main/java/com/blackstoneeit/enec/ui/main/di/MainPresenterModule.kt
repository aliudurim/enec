package com.blackstoneeit.enec.ui.main.di

import com.blackstoneeit.enec.api.ApiService
import com.blackstoneeit.enec.ui.main.MainContract
import com.blackstoneeit.enec.ui.main.MainPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MainPresenterModule {
    @Provides
    @Singleton
    fun provideMainPresenter(apiService: ApiService):
            MainContract.Presenter = MainPresenter(apiService)
}