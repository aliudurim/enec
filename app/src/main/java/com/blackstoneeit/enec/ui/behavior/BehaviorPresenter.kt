package com.blackstoneeit.enec.ui.behavior

import androidx.appcompat.app.AppCompatActivity
import com.blackstoneeit.enec.api.ApiService
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class BehaviorPresenter @Inject constructor(
    private val apiService: ApiService
) : BehaviorContract.Presenter {

    private val subscriptions = CompositeDisposable()
    private lateinit var mView: BehaviorContract.View

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: BehaviorContract.View) {
        mView = view
    }

    override fun getActivity(activity: AppCompatActivity) {
        mView.showActivity(activity)
    }
}