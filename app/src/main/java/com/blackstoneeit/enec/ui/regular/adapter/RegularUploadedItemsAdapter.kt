package com.blackstoneeit.enec.ui.regular.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.blackstoneeit.enec.R
import com.blackstoneeit.enec.models.IncidentMediaFile
import kotlinx.android.synthetic.main.view_item_regular_uploaded_item.view.*

class RegularUploadedItemsAdapter(val itemClick: (mediaFile: IncidentMediaFile) -> Unit) :
    RecyclerView.Adapter<RegularUploadedItemsViewHolder>() {

    private var filesUploaded = mutableListOf<IncidentMediaFile>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RegularUploadedItemsViewHolder {
        return RegularUploadedItemsViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.view_item_regular_uploaded_item, parent, false)
        )
    }

    override fun getItemCount(): Int = filesUploaded.size

    override fun onBindViewHolder(holder: RegularUploadedItemsViewHolder, position: Int) {
        holder.itemView.edRegularUploadedTitle.text = filesUploaded[holder.adapterPosition].fileName
        holder.itemView.edRegularUploadedDesc.text =
            String.format(filesUploaded[holder.adapterPosition].fileExtension + " FILE")
        holder.itemView.setOnClickListener {
            itemClick(filesUploaded[holder.adapterPosition])
        }
    }

    fun setUploadedItems(list: List<IncidentMediaFile>) {
        filesUploaded.clear()
        filesUploaded.addAll(list)
        notifyDataSetChanged()
    }
}

class RegularUploadedItemsViewHolder(inView: View) : RecyclerView.ViewHolder(inView)