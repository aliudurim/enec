package com.blackstoneeit.enec.ui.splash.di

import com.blackstoneeit.enec.di.scopes.ActivityScope
import com.blackstoneeit.enec.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SplashModule {
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeSplashActivityInjector(): SplashActivity
}