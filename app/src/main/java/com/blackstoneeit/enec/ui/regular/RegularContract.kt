package com.blackstoneeit.enec.ui.regular

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.blackstoneeit.enec.ui.base.BaseContract

class RegularContract {
    interface View : BaseContract.View {
        fun showProgress(show: Boolean)
        fun showErrorMessage(error: String)
        fun showActivity(activity: AppCompatActivity)
        fun showRegularCommittedList(recyclerView: RecyclerView)
        fun showRegularUploadedItemsList(recyclerView: RecyclerView)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun getActivity(activity: AppCompatActivity)
        fun getRegularCommittedList(recyclerView: RecyclerView)
        fun getRegularUploadedItemsList(recyclerView: RecyclerView)
    }
}