package com.blackstoneeit.enec.ui.main.di

import com.blackstoneeit.enec.di.scopes.ActivityScope
import com.blackstoneeit.enec.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainModule {
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMainActivityInjector(): MainActivity
}