package com.blackstoneeit.enec.ui.behavior.di

import com.blackstoneeit.enec.di.scopes.ActivityScope
import com.blackstoneeit.enec.ui.behavior.BehaviorActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BehaviorModule {
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeBehaviorActivityInjector(): BehaviorActivity
}