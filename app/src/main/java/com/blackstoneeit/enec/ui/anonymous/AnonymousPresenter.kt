package com.blackstoneeit.enec.ui.anonymous

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.blackstoneeit.enec.api.ApiService
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AnonymousPresenter @Inject constructor(
    private val apiService: ApiService
) : AnonymousContract.Presenter {

    private val subscriptions = CompositeDisposable()
    private lateinit var mView: AnonymousContract.View

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: AnonymousContract.View) {
        mView = view
    }

    override fun getActivity(activity: AppCompatActivity) {
        mView.showActivity(activity)
    }

    override fun getIncidentTypeList(recyclerView: RecyclerView) {
        mView.showIncidentTypeList(recyclerView)
    }
}