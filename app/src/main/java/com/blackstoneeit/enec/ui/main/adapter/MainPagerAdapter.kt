package com.blackstoneeit.enec.ui.main.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.blackstoneeit.enec.ui.anonymous.AnonymousFragment
import com.blackstoneeit.enec.ui.regular.RegularFragment

class MainPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return AnonymousFragment()
            1 -> return RegularFragment()
        }
        return AnonymousFragment()
    }

    override fun getCount(): Int {
        return 2
    }
}