package com.blackstoneeit.enec.ui.main

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.blackstoneeit.enec.api.ApiService
import com.blackstoneeit.enec.models.request.RequestEnvelope
import com.blackstoneeit.enec.utils.TempNetworkModule
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainPresenter @Inject constructor(
    private val apiService: ApiService
) : MainContract.Presenter {

    private val subscriptions = CompositeDisposable()
    private lateinit var mView: MainContract.View

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: MainContract.View) {
        mView = view
    }

    override fun getActivity(activity: AppCompatActivity) {
        mView.showActivity(activity)
    }

    override fun getViewPager(viewPager: ViewPager) {
        mView.showViewPager(viewPager)
    }

    override fun getEnvelope(
        context: Context,
        fullUrlPath: String,
        headers: Map<String, String>,
        requestEnvelope: RequestEnvelope
    ) {
        val apiService = TempNetworkModule.initRetrofitCall(context, fullUrlPath)
        val subscribe = apiService.requestStateInfo(headers, requestEnvelope)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                mView.showProgress(false)
                mView.showEnvelope(it)
            }, {
                mView.showProgress(false)
                it.message?.let { error -> mView.showErrorMessage(error) }
            })
        subscriptions.addAll(subscribe)
    }
}