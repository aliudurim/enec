package com.blackstoneeit.enec.ui.anonymous

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blackstoneeit.enec.R
import com.blackstoneeit.enec.ui.anonymous.adapter.AnonymousIncidentTypeAdapter
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_anonymous.*
import java.util.*
import javax.inject.Inject
import java.text.SimpleDateFormat

class AnonymousFragment : Fragment(), AnonymousContract.View,
    View.OnClickListener, DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {

    companion object {
        fun newInstance(): AnonymousFragment {
            return AnonymousFragment()
        }
    }

    @Inject
    lateinit var presenter: AnonymousContract.Presenter

    private val myCalendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_anonymous, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)
        presenter.subscribe()
        presenter.getIncidentTypeList(rcVAnonymousTypeOfIncident)
        edAnonymousDateOfIncident.setOnClickListener(this)
        edAnonymousTimeOfIncident.setOnClickListener(this)
        rgAnonymous.setOnCheckedChangeListener { group, checkedId ->
            group.check(checkedId)
        }
    }

    override fun showProgress(show: Boolean) {

    }

    override fun showErrorMessage(error: String) {

    }

    override fun showActivity(activity: AppCompatActivity) {

    }

    override fun showIncidentTypeList(recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = AnonymousIncidentTypeAdapter()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.edAnonymousDateOfIncident -> {
                context?.let {
                    DatePickerDialog(
                        it, this, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)
                    ).show()
                }
            }
            R.id.edAnonymousTimeOfIncident -> {
                context?.let {
                    TimePickerDialog(
                        it, this, myCalendar.get(Calendar.HOUR_OF_DAY),
                        myCalendar.get(Calendar.MINUTE), true
                    ).show()
                }
            }
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, monthOfYear)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        updateDateLabel()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        myCalendar.set(Calendar.MINUTE, minute)
        updateTimeLabel()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribe()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private fun updateDateLabel() {
        val myFormat = "MM/dd/yy"
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())

        edAnonymousDateOfIncident.text = sdf.format(myCalendar.time)
    }

    private fun updateTimeLabel() {
        val myFormat = "HH:mm"
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())

        edAnonymousTimeOfIncident.text = sdf.format(myCalendar.time)
    }
}