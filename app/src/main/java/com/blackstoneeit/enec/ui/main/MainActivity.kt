package com.blackstoneeit.enec.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.blackstoneeit.enec.R
import com.blackstoneeit.enec.models.IncidentMediaFile
import com.blackstoneeit.enec.models.request.*
import com.blackstoneeit.enec.models.response.ResponseEnvelope
import com.blackstoneeit.enec.ui.anonymous.AnonymousFragment
import com.blackstoneeit.enec.ui.main.adapter.MainPagerAdapter
import com.blackstoneeit.enec.ui.regular.RegularFragment
import com.blackstoneeit.enec.utils.AppPreferences
import com.blackstoneeit.enec.utils.convertToBase64
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_anonymous.*
import kotlinx.android.synthetic.main.fragment_regular.*
import pl.aprilapps.easyphotopicker.*
import java.util.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainContract.View, View.OnClickListener {

    @Inject
    lateinit var presenter: MainContract.Presenter

    lateinit var easyImage: EasyImage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_main)
        presenter.attach(this)
        presenter.subscribe()
        presenter.getViewPager(mainViewPager)
        mainBtn.setOnClickListener(this)

        easyImage = EasyImage.Builder(this)
            .setChooserType(ChooserType.CAMERA_AND_GALLERY)
            .setFolderName(R.string.app_name.toString())
            .build()
    }

    override fun showProgress(show: Boolean) {

    }

    override fun showErrorMessage(error: String) {
        Snackbar.make(
            mainBtn, error,
            Snackbar.LENGTH_LONG
        ).show()
    }

    override fun showActivity(activity: AppCompatActivity) {

    }

    override fun showEnvelope(responseEnvelope: ResponseEnvelope) {
        Snackbar.make(
            mainBtn, R.string.incident_report,
            Snackbar.LENGTH_LONG
        ).show()
    }

    override fun showViewPager(viewPager: ViewPager) {
        viewPager.adapter = MainPagerAdapter(supportFragmentManager)
        dotsIndicator.setViewPager(viewPager)
        mainBtn.tag = 0
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        mainBtn.tag = 0
                        mainBtn.text = getString(R.string.main_next_step)
                    }
                    1 -> {
                        mainBtn.tag = 1
                        mainBtn.text = getString(R.string.main_submit)
                    }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        easyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
            override fun onMediaFilesPicked(imageFiles: Array<MediaFile>, source: MediaSource) {
                val tempFiles = mutableListOf<IncidentMediaFile>()
                supportFragmentManager.fragments.forEachIndexed { index, fragment ->
                    if (fragment is RegularFragment) {

                        val regularFragment = supportFragmentManager.fragments[index] as RegularFragment
                        imageFiles.forEachIndexed { _, mediaFile ->
                            tempFiles.add(
                                com.blackstoneeit.enec.models.IncidentMediaFile(
                                    mediaFile.file.name,
                                    mediaFile.file.extension,
                                    convertToBase64(mediaFile.file)
                                )
                            )
                        }
                        regularFragment.updateFilesUploaded(tempFiles)
                    }
                }
            }

            override fun onImagePickerError(error: Throwable, source: MediaSource) {
                error.printStackTrace()
            }

            override fun onCanceled(source: MediaSource) {

            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.mainBtn -> {
                when (mainBtn.tag) {
                    0 -> mainViewPager.currentItem = 1
                    1 -> reportIncident()
                }
            }
        }
    }

    override fun onBackPressed() {
        if (mainViewPager.currentItem == 1) {
            mainViewPager.currentItem = 0
        } else {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribe()
    }

    private fun reportIncident() {

        val requestEnvelop = RequestEnvelope()
        val requestBody = RequestBody()
        val reportIncident = ReportIncident()
        val reportIncidentRequest = ReportIncidentRequest()

        val reportIncidentRequestHeader = ReportIncidentRequestHeader()
        reportIncidentRequestHeader.sourceSystemId = ""
        reportIncidentRequestHeader.serverInfo = ""
        reportIncidentRequestHeader.transactionId = ""
        reportIncidentRequestHeader.startTime = Date()
        reportIncidentRequestHeader.endTime = Date()
        reportIncidentRequestHeader.serviceName = ""
        reportIncidentRequestHeader.operationType = ""
        reportIncidentRequestHeader.managerName = ""
        reportIncidentRequestHeader.userName = ""
        reportIncidentRequest.header = reportIncidentRequestHeader

        val reportIncidentRequestBody = ReportIncidentRequestBody()
        supportFragmentManager.fragments.forEachIndexed { _, fragment ->
            if (fragment is RegularFragment) {
                reportIncidentRequestBody.requesterName = "Enec"
                reportIncidentRequestBody.employeeInfo = fragment.edRegularInformationCommitted.text.toString()
                reportIncidentRequestBody.reasonTypeId = ""
                reportIncidentRequestBody.incidentDetails = fragment.edRegularDescriptionEvent.text.toString()
                reportIncidentRequestBody.attachments = fragment.getAttachmentsList()
            } else if (fragment is AnonymousFragment) {
                reportIncidentRequestBody.isAnonymous = fragment.rbAnonymous.isChecked.toString()
                reportIncidentRequestBody.employeeName = fragment.edAnonymousName.text.toString()
                reportIncidentRequestBody.incidentId = "123"
                reportIncidentRequestBody.reportedDate = fragment.edAnonymousDateOfIncident.text.toString()
                reportIncidentRequestBody.location = "Dubai"
            }
        }

        reportIncidentRequest.body = reportIncidentRequestBody
        reportIncident.reportIncidentRequest = reportIncidentRequest
        requestBody.reportIncident = reportIncident
        requestEnvelop.body = requestBody

        val map = HashMap<String, String>()
        map["Content-Type"] = "text/xml;charset=UTF-8"
        map["SOAPAction"] = AppPreferences.getSoapAction()

        val fullUrlPath = AppPreferences.getBaseUrl()

        presenter.getEnvelope(applicationContext, fullUrlPath, map, requestEnvelop)
    }
}
