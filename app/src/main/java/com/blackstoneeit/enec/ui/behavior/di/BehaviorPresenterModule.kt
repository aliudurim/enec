package com.blackstoneeit.enec.ui.behavior.di

import com.blackstoneeit.enec.api.ApiService
import com.blackstoneeit.enec.ui.behavior.BehaviorContract
import com.blackstoneeit.enec.ui.behavior.BehaviorPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class BehaviorPresenterModule {
    @Provides
    @Singleton
    fun provideBehaviorPresenter(apiService: ApiService):
            BehaviorContract.Presenter = BehaviorPresenter(apiService)
}