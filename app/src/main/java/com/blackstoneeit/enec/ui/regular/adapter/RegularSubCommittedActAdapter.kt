package com.blackstoneeit.enec.ui.regular.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.blackstoneeit.enec.R

class RegularSubCommittedActAdapter :
    RecyclerView.Adapter<RegularSubCommittedActViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RegularSubCommittedActViewHolder {
        return RegularSubCommittedActViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.view_item_regular_sub_committed_act, parent, false)
        )
    }

    override fun getItemCount(): Int = 2

    override fun onBindViewHolder(holder: RegularSubCommittedActViewHolder, position: Int) {

    }
}

class RegularSubCommittedActViewHolder(inView: View) : RecyclerView.ViewHolder(inView)