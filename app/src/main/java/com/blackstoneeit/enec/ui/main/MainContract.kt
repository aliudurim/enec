package com.blackstoneeit.enec.ui.main

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.blackstoneeit.enec.models.request.RequestEnvelope
import com.blackstoneeit.enec.models.response.ResponseEnvelope
import com.blackstoneeit.enec.ui.base.BaseContract

class MainContract {
    interface View : BaseContract.View {
        fun showProgress(show: Boolean)
        fun showErrorMessage(error: String)
        fun showActivity(activity: AppCompatActivity)
        fun showViewPager(viewPager: ViewPager)
        fun showEnvelope(responseEnvelope: ResponseEnvelope)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun getActivity(activity: AppCompatActivity)
        fun getViewPager(viewPager: ViewPager)
        fun getEnvelope(
            context: Context,
            fullUrlPath: String,
            headers: Map<String, String>,
            requestEnvelope: RequestEnvelope
        )
    }
}