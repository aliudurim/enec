package com.blackstoneeit.enec.ui.regular.di

import com.blackstoneeit.enec.api.ApiService
import com.blackstoneeit.enec.ui.regular.RegularContract
import com.blackstoneeit.enec.ui.regular.RegularPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RegularPresenterModule {
    @Provides
    @Singleton
    fun provideRegularPresenter(apiService: ApiService):
            RegularContract.Presenter = RegularPresenter(apiService)
}