package com.blackstoneeit.enec.ui.regular.di

import com.blackstoneeit.enec.di.scopes.FragmentScope
import com.blackstoneeit.enec.ui.regular.RegularFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class RegularModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeRegularFragmentInjector(): RegularFragment
}