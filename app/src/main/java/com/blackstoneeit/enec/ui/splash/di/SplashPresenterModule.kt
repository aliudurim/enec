package com.blackstoneeit.enec.ui.splash.di

import com.blackstoneeit.enec.api.ApiService
import com.blackstoneeit.enec.ui.splash.SplashContract
import com.blackstoneeit.enec.ui.splash.SplashPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SplashPresenterModule {
    @Provides
    @Singleton
    fun provideSplashPresenter(apiService: ApiService):
            SplashContract.Presenter = SplashPresenter(apiService)
}