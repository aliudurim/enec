package com.blackstoneeit.enec.ui.anonymous

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.blackstoneeit.enec.ui.base.BaseContract

class AnonymousContract {
    interface View : BaseContract.View {
        fun showProgress(show: Boolean)
        fun showErrorMessage(error: String)
        fun showActivity(activity: AppCompatActivity)
        fun showIncidentTypeList(recyclerView: RecyclerView)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun getActivity(activity: AppCompatActivity)
        fun getIncidentTypeList(recyclerView: RecyclerView)
    }
}