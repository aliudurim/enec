package com.blackstoneeit.enec.ui.anonymous.di

import com.blackstoneeit.enec.di.scopes.FragmentScope
import com.blackstoneeit.enec.ui.anonymous.AnonymousFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AnonymousModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeAnonymousFragmentInjector(): AnonymousFragment
}