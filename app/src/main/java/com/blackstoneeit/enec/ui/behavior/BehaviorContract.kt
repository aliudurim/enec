package com.blackstoneeit.enec.ui.behavior

import androidx.appcompat.app.AppCompatActivity
import com.blackstoneeit.enec.ui.base.BaseContract

class BehaviorContract {
    interface View : BaseContract.View {
        fun showProgress(show: Boolean)
        fun showErrorMessage(error: String)
        fun showActivity(activity: AppCompatActivity)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun getActivity(activity: AppCompatActivity)
    }
}