package com.blackstoneeit.enec.ui.behavior

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.blackstoneeit.enec.R
import com.blackstoneeit.enec.ui.dynamicallyapicall.DynamicallyApiCallActivity
import com.blackstoneeit.enec.ui.main.MainActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_behavior.*
import javax.inject.Inject

class BehaviorActivity : AppCompatActivity(), BehaviorContract.View, View.OnClickListener {

    @Inject
    lateinit var presenter: BehaviorContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_behavior)
        presenter.attach(this)
        presenter.subscribe()
        cVCreateBehavior.setOnClickListener(this)
        btnChangeApiCall.setOnClickListener(this)
    }


    override fun showProgress(show: Boolean) {

    }

    override fun showErrorMessage(error: String) {

    }

    override fun showActivity(activity: AppCompatActivity) {
        val intent = Intent(this@BehaviorActivity, activity::class.java)
        startActivity(intent)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.cVCreateBehavior -> presenter.getActivity(MainActivity())
            R.id.btnChangeApiCall -> presenter.getActivity(DynamicallyApiCallActivity())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribe()
    }
}