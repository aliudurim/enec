package com.blackstoneeit.enec.ui.anonymous.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blackstoneeit.enec.R
import com.blackstoneeit.enec.models.IncidentType
import kotlinx.android.synthetic.main.view_item_anonymous_incident_type.view.*

class AnonymousIncidentTypeAdapter :
    RecyclerView.Adapter<AnonymousIncidentTypeViewHolder>() {

    private val list: MutableList<IncidentType> = mutableListOf(
        IncidentType(false, "Select One"),
        IncidentType(false, "Select Two"),
        IncidentType(false, "Select Three")
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnonymousIncidentTypeViewHolder {
        val view = AnonymousIncidentTypeViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.view_item_anonymous_incident_type, parent, false)
        )
        view.itemView.rcVAnonymousSubIncidentType.layoutManager = LinearLayoutManager(parent.context)
        view.itemView.rcVAnonymousSubIncidentType.adapter = AnonymousSubIncidentTypeAdapter()
        return view
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: AnonymousIncidentTypeViewHolder, position: Int) {
        holder.itemView.edAnonymousIncidentType.text = list[holder.adapterPosition].categoryName
        if (list[holder.adapterPosition].expanded) {
            holder.itemView.cVAnonymousIncidentType.visibility = View.VISIBLE
            holder.itemView.edAnonymousIncidentType.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_arrow_up,
                0
            )
        } else {
            holder.itemView.cVAnonymousIncidentType.visibility = View.GONE
            holder.itemView.edAnonymousIncidentType.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_arrow_down,
                0
            )
        }
        holder.itemView.edAnonymousIncidentType.setOnClickListener {
            if (list[holder.adapterPosition].expanded) {
                list[holder.adapterPosition].expanded = false
            } else {
                list.forEachIndexed { index, categoriesAndServices ->
                    if (categoriesAndServices.expanded) {
                        categoriesAndServices.expanded = false
                        notifyItemChanged(index)
                    }
                }
                list[holder.adapterPosition].expanded = true
            }
            notifyItemChanged(holder.adapterPosition)
        }
    }

}

class AnonymousIncidentTypeViewHolder(inView: View) : RecyclerView.ViewHolder(inView)