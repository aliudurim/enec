package com.blackstoneeit.enec.ui.regular

import android.Manifest
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blackstoneeit.enec.R
import com.blackstoneeit.enec.models.IncidentMediaFile
import com.blackstoneeit.enec.models.request.ReportIncidentRequestAttachments
import com.blackstoneeit.enec.ui.main.MainActivity
import com.blackstoneeit.enec.ui.regular.adapter.RegularCommittedActAdapter
import com.blackstoneeit.enec.utils.openSettings
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_regular.*
import javax.inject.Inject
import com.blackstoneeit.enec.ui.regular.adapter.RegularUploadedItemsAdapter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

class RegularFragment : Fragment(), RegularContract.View,
    View.OnClickListener, MultiplePermissionsListener {

    companion object {
        fun newInstance(): RegularFragment {
            return RegularFragment()
        }
    }

    @Inject
    lateinit var presenter: RegularContract.Presenter
    private lateinit var uploadedItemsAdapter: RegularUploadedItemsAdapter
    private var filesUploaded = mutableListOf<IncidentMediaFile>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_regular, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)
        presenter.subscribe()
        presenter.getRegularCommittedList(rcVRegularCommitted)
        presenter.getRegularUploadedItemsList(rcVRegularUploadedItems)
        cVRegularAddFiles.setOnClickListener(this)
    }

    override fun showProgress(show: Boolean) {

    }

    override fun showErrorMessage(error: String) {

    }

    override fun showActivity(activity: AppCompatActivity) {

    }

    override fun showRegularCommittedList(recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = RegularCommittedActAdapter()
    }

    override fun showRegularUploadedItemsList(recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(context)
        uploadedItemsAdapter = RegularUploadedItemsAdapter {
            filesUploaded.remove(it)
            uploadedItemsAdapter.setUploadedItems(filesUploaded)
        }
        recyclerView.adapter = uploadedItemsAdapter
        uploadedItemsAdapter.setUploadedItems(filesUploaded)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.cVRegularAddFiles -> {
                Dexter.withActivity(activity)
                    .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                    )
                    .withListener(this)
                    .check()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribe()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onPermissionRationaleShouldBeShown(
        permissions: MutableList<PermissionRequest>?,
        token: PermissionToken?
    ) {
        token?.continuePermissionRequest()
    }

    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
        if (report?.areAllPermissionsGranted()!!) {
            context?.let {
                AlertDialog.Builder(it)
                    .setTitle(R.string.app_name)
                    .setMessage(R.string.picker_image)
                    .setNegativeButton(R.string.take) { _, _ ->
                        (activity as MainActivity).easyImage.openCameraForImage(activity as MainActivity)

                    }
                    .setPositiveButton(R.string.pick) { _, _ ->
                        (activity as MainActivity).easyImage.openGallery(activity as MainActivity)

                    }
                    .show()
            }
        } else if (report.isAnyPermissionPermanentlyDenied) {
            Snackbar.make(
                cVRegularAddFiles,
                R.string.permission_permanently_denied_storage_text,
                Snackbar.LENGTH_LONG
            )
                .setActionTextColor(Color.WHITE)
                .setAction(R.string.permission_settings) {
                    context?.let { ctx -> openSettings(ctx) }
                }
                .show()
        }
    }

    fun updateFilesUploaded(filesUploaded: List<IncidentMediaFile>) {
        this.filesUploaded.addAll(filesUploaded)
        uploadedItemsAdapter.setUploadedItems(this.filesUploaded)
    }

    fun getAttachmentsList(): List<ReportIncidentRequestAttachments> {
        val temp = mutableListOf<ReportIncidentRequestAttachments>()
        filesUploaded.forEachIndexed { _, incidentMediaFile ->
            val reportIncidentRequestAttachments = ReportIncidentRequestAttachments()
            reportIncidentRequestAttachments.fileName = incidentMediaFile.fileName
            reportIncidentRequestAttachments.fileObject = incidentMediaFile.base64
            temp.add(reportIncidentRequestAttachments)
        }
        return temp
    }
}