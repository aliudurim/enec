package com.blackstoneeit.enec.ui.anonymous.di

import com.blackstoneeit.enec.api.ApiService
import com.blackstoneeit.enec.ui.anonymous.AnonymousContract
import com.blackstoneeit.enec.ui.anonymous.AnonymousPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AnonymousPresenterModule {
    @Provides
    @Singleton
    fun provideAnonymousPresenter(apiService: ApiService):
            AnonymousContract.Presenter = AnonymousPresenter(apiService)
}