package com.blackstoneeit.enec.ui.regular

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.blackstoneeit.enec.api.ApiService
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class RegularPresenter @Inject constructor(
    private val apiService: ApiService
) : RegularContract.Presenter {

    private val subscriptions = CompositeDisposable()
    private lateinit var mView: RegularContract.View

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: RegularContract.View) {
        mView = view
    }

    override fun getActivity(activity: AppCompatActivity) {
        mView.showActivity(activity)
    }

    override fun getRegularCommittedList(recyclerView: RecyclerView) {
        mView.showRegularCommittedList(recyclerView)
    }

    override fun getRegularUploadedItemsList(recyclerView: RecyclerView) {
        mView.showRegularUploadedItemsList(recyclerView)
    }
}