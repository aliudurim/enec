package com.blackstoneeit.enec.ui.dynamicallyapicall

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.blackstoneeit.enec.R
import com.blackstoneeit.enec.utils.AppPreferences
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_dynamically_api_call.*
import okhttp3.HttpUrl

class DynamicallyApiCallActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_dynamically_api_call)
        btnSaveChangeApiCall.setOnClickListener(this)

        edApiCall.setText(AppPreferences.getBaseUrl())
        edSoapAction.setText(AppPreferences.getSoapAction())
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnSaveChangeApiCall -> {
                if (edApiCall.text.isNotEmpty() && edSoapAction.text.isNotEmpty()) {
                    val baseUrl = HttpUrl.parse(edApiCall.text.toString())
                    if (baseUrl == null) {
                        Snackbar.make(
                            btnSaveChangeApiCall,
                            R.string.dynamically_api_call_error_base_url, Snackbar.LENGTH_SHORT
                        ).show()
                        return
                    } else {
                        AppPreferences.saveBaseUrl(baseUrl.toString())
                    }

                    AppPreferences.saveSoapAction(edSoapAction.text.toString())
                    Handler().postDelayed({
                        finish()
                    }, 200)
                } else {
                    Snackbar.make(
                        btnSaveChangeApiCall,
                        R.string.dynamically_api_call_error, Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }
}