package com.blackstoneeit.enec.ui.anonymous.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.blackstoneeit.enec.R

class AnonymousSubIncidentTypeAdapter :
    RecyclerView.Adapter<AnonymousSubIncidentTypeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnonymousSubIncidentTypeViewHolder {
        return AnonymousSubIncidentTypeViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.view_item_anonymous_sub_incident_type, parent, false)
        )
    }

    override fun getItemCount(): Int = 2

    override fun onBindViewHolder(holder: AnonymousSubIncidentTypeViewHolder, position: Int) {

    }
}

class AnonymousSubIncidentTypeViewHolder(inView: View) : RecyclerView.ViewHolder(inView)