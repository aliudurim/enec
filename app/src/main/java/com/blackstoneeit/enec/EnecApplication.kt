package com.blackstoneeit.enec

import android.app.Activity
import android.app.Application
import androidx.fragment.app.Fragment
import com.blackstoneeit.enec.di.DaggerAppComponent
import com.blackstoneeit.enec.di.modules.NetworkModule
import com.blackstoneeit.enec.ui.anonymous.di.AnonymousPresenterModule
import com.blackstoneeit.enec.ui.behavior.di.BehaviorPresenterModule
import com.blackstoneeit.enec.ui.main.di.MainPresenterModule
import com.blackstoneeit.enec.ui.regular.di.RegularPresenterModule
import com.blackstoneeit.enec.ui.splash.di.SplashPresenterModule
import com.blackstoneeit.enec.utils.AppPreferences
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class EnecApplication : Application(), HasActivityInjector, HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>
    @Inject
    lateinit var dispatchingFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        AppPreferences.init(this)
        DaggerAppComponent.builder()
            .splashPresenterModule(SplashPresenterModule())
            .behaviorPresenterModule(BehaviorPresenterModule())
            .mainPresenterModule(MainPresenterModule())
            .anonymousPresenterModule(AnonymousPresenterModule())
            .regularPresenterModule(RegularPresenterModule())
            .networkModule(NetworkModule(this))
            .build()
            .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingActivityInjector
    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingFragmentInjector
}