package com.blackstoneeit.enec.models

data class IncidentType(
    var expanded: Boolean,
    val categoryName: String
)