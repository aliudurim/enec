package com.blackstoneeit.enec.models

data class CommittedAct(
    var expanded: Boolean,
    val categoryName: String
)