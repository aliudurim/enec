package com.blackstoneeit.enec.models.response

import org.simpleframework.xml.Element

data class ReportIncidentResponseStatus @JvmOverloads constructor(
    @field:Element(name = "status")
    var status: String? = null,
    @field:Element(name = "error")
    var error: ReportIncidentResponseStatusError? = null
)