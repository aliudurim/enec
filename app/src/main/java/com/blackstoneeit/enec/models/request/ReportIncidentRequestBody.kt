package com.blackstoneeit.enec.models.request

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList

data class ReportIncidentRequestBody @JvmOverloads constructor(
    @field:Element(name = "requesterName")
    var requesterName: String? = null,
    @field:Element(name = "isAnonymous")
    var isAnonymous: String? = null,
    @field:Element(name = "employeeName")
    var employeeName: String? = null,
    @field:Element(name = "employeeInfo")
    var employeeInfo: String? = null,
    @field:Element(name = "incidentId")
    var incidentId: String? = null,
    @field:Element(name = "reasonTypeId")
    var reasonTypeId: String? = null,
    @field:Element(name = "reportedDate")
    var reportedDate: String? = null,
    @field:Element(name = "location")
    var location: String? = null,
    @field:Element(name = "incidentDetails")
    var incidentDetails: String? = null,
    @field:ElementList(name = "attachments")
    var attachments: List<ReportIncidentRequestAttachments>? = null
)