package com.blackstoneeit.enec.models.response

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "Body")
data class ResponseBody @JvmOverloads constructor(
    @field:Element(name = "ser-root:SerReportIncidentResponse", required = false)
    var reportIncidentResponse: SerReportIncidentResponse? = null
)