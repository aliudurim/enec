package com.blackstoneeit.enec.models.request

import org.simpleframework.xml.Element
import java.util.*

data class ReportIncidentRequestHeader @JvmOverloads constructor(
    @field:Element(name = "sourceSystemId")
    var sourceSystemId: String? = null,
    @field:Element(name = "serverInfo")
    var serverInfo: String? = null,
    @field:Element(name = "transactionId")
    var transactionId: String? = null,
    @field:Element(name = "startTime")
    var startTime: Date? = null,
    @field:Element(name = "endTime")
    var endTime: Date? = null,
    @field:Element(name = "serviceName")
    var serviceName: String? = null,
    @field:Element(name = "operationType")
    var operationType: String? = null,
    @field:Element(name = "managerName")
    var managerName: String? = null,
    @field:Element(name = "userName")
    var userName: String? = null
)