package com.blackstoneeit.enec.models.response

import org.simpleframework.xml.Element
import org.simpleframework.xml.Namespace

@Namespace(reference = "http://ENECVESBHQD01.ENEC.local/ENECBSHumanResourceMgmt.masterRecords.ws.provider:reportIncident")
data class SerReportIncidentResponse @JvmOverloads constructor(
    @field:Element(name = "reportIncidentResponse")
    var reportIncidentResponse: ReportIncidentResponse? = null
)