package com.blackstoneeit.enec.models.request

import org.simpleframework.xml.Element
import org.simpleframework.xml.Namespace

@Namespace(reference = "http://ENECVESBHQD01.ENEC.local/ENECBSHumanResourceMgmt.masterRecords.ws.provider:reportIncident")
data class ReportIncident @JvmOverloads constructor(
    @field:Element(name = "reportIncidentRequest")
    var reportIncidentRequest: ReportIncidentRequest? = null
)