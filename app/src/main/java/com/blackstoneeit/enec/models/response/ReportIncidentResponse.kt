package com.blackstoneeit.enec.models.response

import org.simpleframework.xml.Element

data class ReportIncidentResponse @JvmOverloads constructor(
    @field:Element(name = "header")
    var header: ReportIncidentResponseHeader? = null,
    @field:Element(name = "status")
    var status: ReportIncidentResponseStatus? = null
)