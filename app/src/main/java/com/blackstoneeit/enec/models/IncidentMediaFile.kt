package com.blackstoneeit.enec.models

data class IncidentMediaFile(
    var fileName: String,
    var fileExtension: String,
    var base64: String
)
