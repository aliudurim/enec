package com.blackstoneeit.enec.models.request

import org.simpleframework.xml.Element
import org.simpleframework.xml.Namespace

@Namespace(reference = "")
data class ReportIncidentRequest @JvmOverloads constructor(
    @field:Element(name = "header")
    var header: ReportIncidentRequestHeader? = null,
    @field:Element(name = "body")
    var body: ReportIncidentRequestBody? = null
)