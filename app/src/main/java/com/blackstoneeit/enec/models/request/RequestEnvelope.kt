package com.blackstoneeit.enec.models.request

import org.simpleframework.xml.Element
import org.simpleframework.xml.Namespace
import org.simpleframework.xml.NamespaceList
import org.simpleframework.xml.Root

@Root(name = "soapenv:Envelope")
@NamespaceList(
    Namespace(reference = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi"),
    Namespace(reference = "http://www.w3.org/2001/XMLSchema", prefix = "xsd"),
    Namespace(reference = "http://schemas.xmlsoap.org/soap/encoding/", prefix = "enc"),
    Namespace(reference = "http://schemas.xmlsoap.org/soap/envelope/", prefix = "soapenv")
)
data class RequestEnvelope @JvmOverloads constructor(
    @field:Element(name = "soapenv:Body", required = false)
    var body: RequestBody? = null
)