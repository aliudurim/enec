package com.blackstoneeit.enec.models.response

import org.simpleframework.xml.Element

data class ReportIncidentResponseStatusError @JvmOverloads constructor(
    @field:Element(name = "errorSourceSystem")
    var errorSourceSystem: String? = null,
    @field:Element(name = "errorCode")
    var errorCode: String? = null,
    @field:Element(name = "errorType")
    var errorType: String? = null,
    @field:Element(name = "errorDescription")
    var errorDescription: String? = null
)