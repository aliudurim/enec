package com.blackstoneeit.enec.models.request

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "soapenv:Body", strict = false)
data class RequestBody @JvmOverloads constructor(
    @field:Element(name = "enec:reportIncident", required = false)
    var reportIncident: ReportIncident? = null
)