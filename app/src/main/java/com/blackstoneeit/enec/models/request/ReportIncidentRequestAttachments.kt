package com.blackstoneeit.enec.models.request

import org.simpleframework.xml.Element

data class ReportIncidentRequestAttachments @JvmOverloads constructor(
    @field:Element(name = "fileName")
    var fileName: String? = null,
    @field:Element(name = "fileObject")
    var fileObject: String? = null
)