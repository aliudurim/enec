package com.blackstoneeit.enec.di

import com.blackstoneeit.enec.EnecApplication
import com.blackstoneeit.enec.di.modules.NetworkModule
import com.blackstoneeit.enec.ui.anonymous.di.AnonymousModule
import com.blackstoneeit.enec.ui.anonymous.di.AnonymousPresenterModule
import com.blackstoneeit.enec.ui.behavior.di.BehaviorModule
import com.blackstoneeit.enec.ui.behavior.di.BehaviorPresenterModule
import com.blackstoneeit.enec.ui.dynamicallyapicall.di.DynamicallyApiCallModule
import com.blackstoneeit.enec.ui.main.di.MainModule
import com.blackstoneeit.enec.ui.main.di.MainPresenterModule
import com.blackstoneeit.enec.ui.regular.di.RegularModule
import com.blackstoneeit.enec.ui.regular.di.RegularPresenterModule
import com.blackstoneeit.enec.ui.splash.di.SplashModule
import com.blackstoneeit.enec.ui.splash.di.SplashPresenterModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        SplashModule::class,
        SplashPresenterModule::class,
        BehaviorModule::class,
        BehaviorPresenterModule::class,
        MainModule::class,
        MainPresenterModule::class,
        AnonymousModule::class,
        AnonymousPresenterModule::class,
        RegularModule::class,
        RegularPresenterModule::class,
        DynamicallyApiCallModule::class,
        NetworkModule::class
    ]
)
interface AppComponent {
    fun inject(app: EnecApplication)
}