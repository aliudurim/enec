package com.blackstoneeit.enec.api

import com.blackstoneeit.enec.models.request.RequestEnvelope
import com.blackstoneeit.enec.models.response.ResponseEnvelope
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.HeaderMap
import retrofit2.http.POST

interface ApiService {
    @POST(".")
    fun requestStateInfo(
        @HeaderMap headers: Map<String, String>,
        @Body body: RequestEnvelope
    ): Observable<ResponseEnvelope>
}